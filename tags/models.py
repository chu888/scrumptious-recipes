from django.db import models
from recipes.models import Recipe


class Tags(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField(
        Recipe,
        related_name='tags'
        )

    def __str__(self):
        print(type(self.recipes))
        return f'{self.name} is tagged with {self.recipes}'
