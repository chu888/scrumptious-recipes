from django.contrib import admin
from .models import FoodItem, Measure, Recipe, Step, Ingredient


admin.site.register(Recipe)
admin.site.register(Measure)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
admin.site.register(Step)


 