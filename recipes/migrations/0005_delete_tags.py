# Generated by Django 4.0.3 on 2022-07-14 23:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_step_food_items'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Tags',
        ),
    ]
